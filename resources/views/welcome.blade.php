<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('CSS/style.css')}}">
    <!-- Google Fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('IMG/favicon.png')}}" type="image/x-icon">

    <title>Registration Form</title>
</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 mt-4 mt-2">
                <!-- Mensaje de confirmación -->
                @if (session()->get('success'))
                <div class="alert alert-success alert-dismissible fade show mt-5" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                @if (session()->get('error'))
                <div class="alert alert-danger alert-dismissible fade show mt-5" role="alert">
                    {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="card mt-5 rounded">
                    <div class="card-header text-center">
                        <h1><b>Formulario de Registro</b></h1>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('usuarios.store') }}" method="POST">
                            @CSRF
                            <div class="row">
                                <div class="col-md-6 mt-2">
                                    <label for="names">Nombres:</label>
                                    <input type="text" class="form-control rounded" placeholder="Ingresa tu nombre" name="names" value="{{old('names')}}">
                                    @error('names')
                                    <small>*{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-2">
                                    <label for="last_names">Apellidos:</label>
                                    <input type="text" class="form-control rounded" placeholder="Ingresa tu apellido" name="last_names" value="{{old('last_names')}}">
                                    @error('last_names')
                                    <small>*{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-2">
                                    <label for="type_document">Tipo de documento:</label>
                                    <select id="type_document" name="tpo_document" class="form-control rounded">
                                        <option selected disabled>Seleccionar opción</option>
                                        <option value="1">Tarjeta de identidad</option>
                                        <option value="2">Cédula de ciudadania</option>
                                        <option value="3">Cédula de extranjería</option>
                                        <option value="4">Pasaporte</option>
                                        <option value="5">Otro...</option>
                                    </select>
                                    @error('tpo_document')
                                    <small>*{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-2">
                                    <label for="identification_card">Número de documento:</label>
                                    <input type="number" class="form-control rounded" placeholder="Ingresa el número de documento" name="num_document" value="{{old('num_document')}}">
                                    @error('num_document')
                                    <small>*{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-2">
                                    <label for="phone">Teléfono:</label>
                                    <input type="number" class="form-control rounded" placeholder="Ingresa tu número de teléfono" name="phone" value="{{old('phone')}}">
                                    @error('phone')
                                    <small>*{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-5 text-center">
                                    <input type="reset" value="Limpiar" class="btn btn-secondary rounded">
                                    <input type="submit" value="Enviar" class="btn btn-dark rounded btn-color">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>

</html>