<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validació de campos requeridos
        $request->validate([
            'names' => 'required',
            'last_names' => 'required',
            'tpo_document' => 'required',
            'num_document' => 'required',
            'phone' => 'required',
        ]);
        
        // dd($request->num_document);
        $data = DB::table('users')
            ->select('num_document')
            ->where('num_document', '=', $request->num_document)
            ->first();
    
        if ($data) {
          
            $typeMessage = 'error';
            $message = 'El número de documento ya se encuentra registrado';
        }else {

            $users = User::create([
                'names' => $request->get('names'),
                'last_names' => $request->get('last_names'),
                'tpo_document' => $request->get('tpo_document'),
                'num_document' => $request->get('num_document'),
                'phone' => $request->get('phone')
            ]);
            $users->save();
            $typeMessage = 'success';
            $message = 'Exelente !! Tu registo fue almacenado en nuestra base de datos';
        }
        return redirect()->route('usuarios.index')->with($typeMessage,$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
